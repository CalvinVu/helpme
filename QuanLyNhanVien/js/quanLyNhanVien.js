//TIM VI TRI BANG STRING VOI FINDINDEX ????

//DANH SACH SINH VIEN
var danhSachNhanVien = [];

// LAY DU LIEU TU LOCAL
var dataLocal = localStorage.getItem("DANH_SACH_NHAN_VIEN");
if (dataLocal != null) {
  danhSachNhanVien = JSON.parse(dataLocal);
  renderNhanVien();
}

//THEM NHAN VIEN
function themNhanVien() {
  var taiKhoan = document.getElementById("tknv").value;
  var tenNhanVien = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  var ngayLamViec = document.getElementById("datepicker").value;
  var luongCoBan = document.getElementById("luongCB").value;
  var chucVu = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("gioLam").value;
  var nhanVien = {
    taiKhoan: taiKhoan,
    tenNhanVien: tenNhanVien,
    email: email,
    password: password,
    ngayLamViec: ngayLamViec,
    luongCoBan: luongCoBan,
    chucVu: chucVu,
    gioLam: gioLam,
  };
  var isVali = valiTaiKhoan();
  if (isVali === true) {
    danhSachNhanVien.push(nhanVien);
    console.log(danhSachNhanVien);
    renderNhanVien();
    localDSSV();
  }
  return nhanVien;
}

// RENDER SINH VIEN
function renderNhanVien() {
  var HTML = "";
  for (var i = 0; i < danhSachNhanVien.length; i++) {
    var item = danhSachNhanVien[i];
    HTML += `<tr>
    <td>${item.taiKhoan}</td>
    <td>${item.tenNhanVien}</td>
    <td>${item.email}</td>
    <td>${item.ngayLamViec}</td>
    <td>${item.chucVu}</td>
    <td></td>
    <td></td>
    <td><button class="btn btn-danger" onclick="xoaNhanVien(${item.taiKhoan})">XOA</button>
    <button class="btn btn-success" id="suaNhanVien" onclick="suaNhanVien(${item.taiKhoan})" data-toggle="modal" data-target="#myModal">SUA</button>
    </tr>`;
    document.getElementById("tableDanhSach").innerHTML = HTML;
  }
}

// LUU XUONG LOCAL STORAGE
function localDSSV() {
  var dataLocal = JSON.stringify(danhSachNhanVien);
  localStorage.setItem("DANH_SACH_NHAN_VIEN", dataLocal);
}

// XOA NHAN VIEN
function xoaNhanVien(id) {
  var viTri = danhSachNhanVien.findIndex(function (item) {
    return item.taiKhoan == id;
  });
  console.log(viTri);
  if (viTri != -1) {
    danhSachNhanVien.splice(viTri, 1);
    renderNhanVien();
    localDSSV();
  }
}

// SUA NHAN VIEN
function suaNhanVien(id) {
  var viTri = danhSachNhanVien.findIndex(function (item) {
    return item.taiKhoan == id;
  });
  console.log(viTri);
  document.getElementById("tknv").value = danhSachNhanVien[viTri].taiKhoan;
  document.getElementById("name").value = danhSachNhanVien[viTri].tenNhanVien;
  document.getElementById("email").value = danhSachNhanVien[viTri].email;
  document.getElementById("password").value = danhSachNhanVien;
  document.getElementById("datepicker").value =
    danhSachNhanVien[viTri].ngayLamViec;
  document.getElementById("luongCB").value = danhSachNhanVien[viTri].luongCoBan;
  document.getElementById("chucvu").value = danhSachNhanVien[viTri].chucVu;
  document.getElementById("gioLam").value = danhSachNhanVien[viTri].ngayLamViec;
}

// CAP NHAT SINH VIEN
document.getElementById("btnCapNhat").onclick = function () {
  var nhanVien = layValueTuForm();
  var viTri = danhSachNhanVien.findIndex(function (item) {
    return item.taiKhoan == nhanVien.taiKhoan;
  });
  danhSachNhanVien[viTri] = nhanVien; // NGUY HIEM NEU NGUOC LAI
  renderNhanVien();
  localDSSV();
};

// LAY THONG TIN NHAP TU FORM
function layValueTuForm() {
  var taiKhoan = document.getElementById("tknv").value;
  var tenNhanVien = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  var ngayLamViec = document.getElementById("datepicker").value;
  var luongCoBan = document.getElementById("luongCB").value;
  var chucVu = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("gioLam").value;
  var nhanVien = {
    taiKhoan: taiKhoan,
    tenNhanVien: tenNhanVien,
    email: email,
    password: password,
    ngayLamViec: ngayLamViec,
    luongCoBan: luongCoBan,
    chucVu: chucVu,
    gioLam: gioLam,
  };
  return nhanVien;
}

// VALIDATION FORM TAI KHOAN
function valiTaiKhoan() {
  var taiKhoan = document.getElementById("tknv").value;
  if (taiKhoan.length <= 6) {
    return true;
  } else if (taiKhoan === "" || taiKhoan === null) {
    document.getElementById("tbTKNV").innerHTML = "sai tai khoan";
    return false;
  } else {
    document.getElementById("tbTKNV").innerHTML = "sai tai khoan";
    return false;
  }
}
